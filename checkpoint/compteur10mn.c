#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>  //sleep


/*
 - compteur sur 10 mn (affichage incrementations toutes les secondes)
 - capture de signaux ( pour test de checkpointing avec dmtcp /OAR Caclculco)
 - le compteur est sauvegardé dans un fichier sur capture de signal (SIGINT /2)
     -> à réutiliser pour reprise de travaux après checkpointing ) 
 */
#define SIZE 1
#define NUMELEM 5

/*compteur  variable globale ( main et signal ) */
int compteur;
//findice_ckpt
FILE * fp;


void sig_handler(int signal)
{
switch (signal)
  {
  case SIGINT:
    // code exécuté si on reçoit SIGINt (CTRL+C);        
    printf("SIGINT recu (programme C)\n");
    printf("valeur du compteur enregistre: %d\n", compteur);
    // fichier de sauvegarde du "contexte" ( l'indice = nb de secondes!
    fp = fopen("context_compteur.txt", "wb");
    if(fp == NULL) {
        printf("error creating file");
    }
    else {
      fwrite( &compteur , sizeof(int) , 1 , fp);
      fclose(fp);
    }
    break;

  case SIGUSR2:        
    // code normalement exécuté si on reçoit SIGUSR2 (OAR?) ou SIGUSR1 (dmtcp);
    // NE FONCTIONNE PAS ( c'est donc le SIGINT 2 ci- dessus qui est utlisé dans
    // checkpoint-user1.oar
    printf("signal checkpoint de dmtcp recu \n");
     printf("valeur du compteur: %d\n", compteur);
    fp = fopen("context_compteur.txt", "wb");
    if(fp == NULL) {
        printf("error creating file");
    }
    else {
      fwrite( &compteur , sizeof(int) , 1 , fp);
      fclose(fp);
    }

    break;

    default :
    // sortie
    exit(0);
  }
}


//int main( int argc, char *argv[] )
int main(void)
{
  if (signal(SIGINT, sig_handler) == SIG_ERR)
    printf("\n ne peut pas recevoir SIGINT\n");

  // ----- test si un checkpoint existe ---- 
  // si oui, lecture de la valeur dans le fichier
  int i ;
  fp = fopen("context_compteur.txt", "rb");
  if (fp){
    fread( &i , sizeof(int) , 1 , fp);
    printf("valeur compteur au precedent checkpoint = %d\n", i);
    fclose(fp);
  }else{
    i=0;
  }
  // ---- le Compteur propremen dit ---------
  // 10 mn(environ: ce n'est pas un chrono!)
  
  for (compteur=i; compteur <=600; compteur++)
    {
      printf("compteur = %d\n", compteur);
      // force l'écriture sur la sortie
      fflush(stdout);
      sleep(1);
    }
  return 0;
}
