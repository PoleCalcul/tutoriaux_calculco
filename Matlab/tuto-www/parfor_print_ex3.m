% - ex boucle parallèle: parfor
% - les itérations sont indépendantes, l'efficacité est donc optimale(!)
% - En revanche, les itérations sur le nombre de threads (1, 2, 4, 8)
% sont réalisées avec l'initialisation/réinitialisation d'un pool 
% de coeur à chaque itération (parpool(nb_coeurs)): cette opération est 
% lourde en temps...mais nécessaire car Matlab utilise par défaut le
% nombre max de coeurs alloués/demandés: la fonction maxNumCompThreads est 
% inopérante dans un parfor.

N = 200;
M = 400;
a = zeros(N,1);
core(1)= 1;
speedup(1)=1;
tic;   % serial (regular) for-loop
% calcul séquentiel
for i = 1:N
        a(i) = a(i) + max(eig(rand(M)));
end
time(1) = toc;

for j = 2:5
    %maxNumCompThreads will be removed in a future release ...but replace by?
    %
    core(j) = 2^(j-1);     % 1,2,4,8...
    parpool(core(j));
    fprintf('nombre de threads: %d \n', core);
    tic;   % parallel for-loop
    parfor i = 1:N
            a(i) = a(i) + max(eig(rand(M)));
    end   
    time(j) = toc;
    speedup(j) = time(1)/time(j);
    delete(gcp('nocreate'))
end

 % En mode batch, les figures doivent être sauvées dans des fichiers
 fig = figure;
 plot(core,core,core,speedup,'-*');
 title(' Speedup parfor (exemple 1) : 1,2,..,16 coeurs ');
 xlabel('nombre de coeurs');
 ylabel('Speedup = T_{coeurs} / T_{sequentiel}');
 % sauve figure-20161028-11-24.png
 print(fig, strcat('figure-',datestr(now,'yyyymmdd-HH-MM')),'-dpng');
 % merci de quitter matlab pour libérer les jetons de licence.
 quit 
