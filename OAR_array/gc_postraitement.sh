#!/bin/bash

# script portraitement pour regrouper l'ensemble des résultats
# des jobs gradient conjugué dans un tableau au format csv

# optionnel: récuperation du numéro du job(tableau) lancé
# intérêt: permet rapidement de revisiter les paramètres de lancement,
# dates, temps d'exécution, sur quel nœud de calcul, le job a été
# executé grâce à la commande:
#     oarstat -fj array_id
array_id=`grep  array_id cg.*stdout | head -1 | cut -d: -f3`

echo '"LDA", "tolerance", "maxiter", "iteration", "elapsed"' >> gc_bilan-$array_id.csv
for fic in `ls -1 cg.*.stdout`
do
    grep csv $fic | cut -d: -f2 >> gc_bilan-$array_id.csv
done

# suppression des fichiers de chaque run
rm -f cg.*stdout cg.*stderror

