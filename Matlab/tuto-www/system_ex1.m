% r�solution de Ax = b avec du multithreading
n = 20000;        % taille de la matrice n x n 
P = randn(n);
A = P' + P;       % A matrice sym�trique  
%A = triu(rand(n)); % A triangulaire
x = ones(n,1);    % la solution est le vecteur unit�
b = A*x;          % calcul du rhs

fprintf('\n\n*** resolution systeme lineaire ***\n');
fprintf('\n taille du systeme, n = %d \n\n',n);


tic                     % top horloge
y1 = A\b;               % calcul de la solution (en principe vec unit�!)
time= toc;          % top horloge

fprintf('\n temps execution mldivide: %f \n',time);

opts= struct('SYM', true);
%opts= struct('UT', true);
tic
y2 = linsolve(A,b,opts);
%y2 = linsolve(A,b);
time= toc;          % top horloge

fprintf('\n temps execution linsolve: %f \n',time);

 % merci de quitter matlab pour lib�rer les jetons de licence.
 quit 
