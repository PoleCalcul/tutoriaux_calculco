% Calculs Parall�les avec la Parallel Toolbox
% - mise en oeuvre de spmd (Single Program Multiple Data)  
%  (spmd ~ une version simplifi�e de MPI "sauce Matlab")
%
% Program spmd_ex4_int_trapeze.m: 
%  - Calcul int�gral par la m�thode des trap�zes.
%  https://fr.wikipedia.org/wiki/M�thode_des_trap�zes
%
%
% Remarque: il s'agit d'un programme exemple ... car en matlab
% les deux lignes suivantes suffisent !!! ;-)  : 
%
% f=@(x) 4./(1+x.^2);
% Q=integral(f,0,1)

%function value = spmd_ex4_int_trapeze(n)

% cf tutoriaux parfor_ex2.m: si le nombre de ressources est
% sup�rieur � 12, il faut forcer le pool.
% le script oar matlab_ex4_lauch.oar demande 16 coeurs via
% la variable d environnement number_Threads

if (~isempty(getenv('number_Threads'))) 
     % parpool(16)
     parpool(uint8(str2num(getenv('number_Threads'))))
   else
     % rien, le pool est initialis� au nb de coeurs demand�s
end

%p = gcp;
%p.NumWorkers
n=100;
% cf matlab anonymous function. (ici, (4 x) d�riv� de arctan pour le calcul de pi)
f = @(x) 4./(1+x.^2)

fprintf ( 1 , 'calcul des bornes \n');
  spmd
     a = ( labindex -1 ) / numlabs ;
     b = labindex/ numlabs ;
fprintf ( 1, 'le Lab %d  travaille sur l intervalle [% f ,% f] \n',labindex ,a ,b ) ;
  end
  fprintf ( 1 , 'chaque workers calcule son aire locale  \n') ;
  spmd
      x = linspace ( a ,b ,n );
      fx = f(x);
      formule_de_quadrature = ( b - a ) * ( (fx(1) + fx(n))/2 +  sum ( fx(2:n-1) ))/(n-1);
      fprintf ( 1 , ' Approximation partielle %f \n', formule_de_quadrature) ;
  end
  %approximation = sum ( formule_de_quadrature{:} ) ;
  approximation = sum(cellfun(@double,formule_de_quadrature(:,:)));
  fprintf ( 1, 'Approximation de pi = %f \n', approximation);

    
% merci de quitter matlab proprement pour lib�rer correctement les jetons
quit
