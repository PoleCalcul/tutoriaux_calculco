## Usage «avancé» OAR : array  

*La* «bonne pratique» pour lancer massivement, en parallèle, le même
programme avec des paramètres différents : **utiliser l'option
`--array-param-file parametres.txt` d'OAR **. Cette option lance un
«tableau de tâches» chacune indépendantes les unes des autres

- développer le programme de tel sorte qu'il prenne les paramètres en argument : ./programme.exe arg1 arg2 arg3...
- générer un fichier ascii qui contient l'ensemble des paramètres à tester
   - arg11 arg12 arg13 ..
   - arg21 arg22 arg23 ..

Note: ce fichier peut contenir des centaines, des milliers de lignes

- lancer les expériences via un script OAR:
   - avec l'option `#OAR --array-param-file parametres.txt`
   - dans la queue *besteffort* 
   - avec l'option *idempotent*

Rappel: en besteffort, les ressources sont illimitées mais les jobs sont non prioritaires (ils peuvent être supprimés sans préavis si des jobs de priorité supérieure sont lancés). À noter dans ce cas que le strict nécessaire aux jobs prioritaires est «libéré»: ce n'est pas le tableau de tâches qui est intégralement supprimé. De plus, l'option *idempotent* permet de relancer automatiquement toutes les expériences (qui auraient été tuées par des jobs prioritaires)


### exemple 1 : pi_array.oar

le programme pi.exe calcule une approximation de pi par l'intégration de f(x)=4/(1+x^2) sur [0;1].
Le nombre d'intervalles est passé en argument (regroupé dans le fichier pi_input.txt)

- make    (compile le programme pi.exe)
- oarsub -S ./pi_array.oar

### exemple 2 (matlab): gradientconj.m

Le programme résout un système linéaire par la méthode itérative du gradient conjugué.
Le script matlab a été tranformé en fonction: gradientconj(n,tolerance,maxiter)

- oarsub -S ./gc_matlab_array.oar

#### description des fichiers

- gc_matlab_array.oar : le script OAR à lancer (oarsub -S ./gc_array.oar)
- gradientconj.m : prog. matlab 
- gc_input.txt : paramètres des différents jobs ( n, tol, maxiter )
   - 1000 1e-5 1000
   - 2000 1e-7 1500
   - 6000 1e-6 6000
   - etc.
- gc_postraitement.sh (optionnel): génère un fichier tableau (csv)
  gc_bilan.csv à partir des fichiers générés par chaque job

     
#### note:  usage de jetons

- comme sur un poste personnel, plusieurs sessions matlab ne «consomme» qu'un seul jeton
- si le fichier gc_input.txt est suffisamment long ( > nombre max de
  cœurs du nœud utilisé ) d'autres nœuds seront utilisés , ainsi que
  d'autres jetons (mais 1 seul jeton/nœud)

### exemple 3 : matmul OpenMP 

Les fichiers sont dans le sous-répertoire "ex3". l'exemple, pour la
démo, est sans grand intérêt (20 fois la même multiplication de deux
matrice A(n,n) x B(n,n) remplies de 1 ...).

Néanmoins, l'usage d'OpenMP est extrêmement performant pour effectuer
ces opérations (tester le programme fortran sur votre poste de travail
en fixant successivment différentes valeurs de la variable
d'environnement OMP_NUM_THREADS pour vous en convaincre: export
OMP_NUM_THREADS=x (x=1,2,4...))


L'exemple est donné ici pour illustrer comment adapter un script OAR
pour une campagne de test ( donc usage d'un *OAR_ARRAY* ) avec un
programme multithreadé, en utilisant de façon optimale un nœud de
calculco: 

1. a minima en s'assurant que les ressources utiles à un job soit
   utilisées par ce dernier et seulement celui-ci.  
2. de plus 

Pour cela: 

- le nœud de calcul utilisé est orval06 ( 2 cpus de 8 cœurs chacuns )
- le nombre de threads est (arbitrairement) choisi à 4
- la réservation des ressources est définie comme suit (extrait): `-l /cpu=1/core=4`.
     - comme s'il n'y avait qu'un seul job ( 4 cœurs pour 4 threads )
     - de plus les quatres cœurs utilisés le seront sur le même processeur ce qui peut-être un facteur d'optimisation. En effet, les nœuds ont tous entre 2 et 4 processeurs. Tous les processeurs ont bien accès à l'ensemble de la mémoire du nœud. Mais la mémoire n'est pas disposée de façon homogène sur les cartes mère: il y a une notion de "proximité à chaque CPU" qui entre en jeu: l'accès mémoire d'un CPU à la mémoire de "proximité" d'un autre CPU a un facteur pénalisant en terme de temps d'accès (cf. architecture [NUMA](https://fr.wikipedia.org/wiki/Non_Uniform_Memory_Access))

Ainsi, quelque soit le nombre de tests à effectuer (donc le nombre de ligne dans le fichier matmul_input.txt), il y a garantie qu'ils seront lancés par lot et que la totalité du nœud orval06 sera utilisé (cpu n°1 : 2 jobs de 4 threads; cpu n°2 : 2 jobs de 4 threads, soit 4 jobs de 4 threads exécutés simultanément sur les 16 coœurs d'orval06, jusqu'à épuisement du fichier paramètres matmul_input.txt)

Rq: compte tenu de «l'efficacité» d'OpenMP pour l'exécution de ce programme fortran, on obtient un temps global du test tout à fait équivalent en les lançant par lot de 2. il suffit de modifier le script matmul_array.oar: 
- `#OAR -l /cpu=1/core=8,walltime=20:00`
- `export OMP_NUM_THREADS=8`