% - exemple de boucle parallèle: parfor
% - les itérations sont indépendantes, l'efficacité est donc optimale(!)
%
%

N = 200;
M = 400;
a = zeros(N,1);

tic;   % serial (regular) for-loop
% calcul séquentiel
for i = 1:N
        a(i) = a(i) + max(eig(rand(M)));
end
time(1) = toc;
fprintf('temps sequentiel: %f \n', time(1));

% Usage de la Parallel Computing Toolbox:
%
% Les fonctionnalités parallèle (parfor, parfeval, spmd ...)
% nécessite l'initialisation d'un "pool de workers" via 
% la commande  "parpool" 
%
% - le nombre de workers ne peut être supérieur au nombre de cœurs  
% - le nombre de workers par défaut est le nombre de coeurs 
% demandés (et max 12) , par conséquent si les ressources demandées 
% via OAR sont supérieures à 12 , il faut forcer  l'usage de
%   ces ressources:
%
%  exemple  (manuellement):  
%   - parpool(16)
%   - (avec 16 cœurs demandés: OAR -l /nodes=1/core=16,walltime=00:20)
%
% exemple 2 (mieux?): récupération du nombre de ressources via une
% variable d'environnement du SHELL passée par exemple dans le 
% script de soumission du jobs   
%    
%  - export number_Threads=16
%  
% Remarque: l'instruction "parpool" est facultative: elle est implicitement  
% lancée dès que l'interpréteur rencontre une instruction propre
% à la Parallèle ToolBox - par ex. parfor - ... (mais le nombre
% de workers sera dans ce cas au plus 12)  
%
% cf. https://fr.mathworks.com/help/distcomp/parpool.html

% Selon la solution adoptée plus haut:

if (isempty(getenv('number_Threads'))) 
     %nb de thread fixé manuellement mais qui...
     % doit être inférieur aux ressources/coeurs demandés!!
     nbcore = 8
   else
     % lecture de la variable SHELL number_Threads 
     nbcore = uint8(str2num(getenv('number_Threads'))) 
end

% initialisation du pool de workers
parpool(nbcore)

tic;   
% parallel for-loop
parfor i = 1:N
      a(i) = a(i) + max(eig(rand(M)));
end   
time(2) = toc;
speedup = time(1)/time(2);

fprintf('temps execution parallele : %f \n', time(2));
fprintf('acceleration du parallelisme: %f \n', speedup);

% Pour infos, si on désire connaître/manipuler les propriétés
% d'un pool (NumWorkers, AttachedFiles,SpmdEnabled etc.),
% il faut utiliser la fonction "get current pool" (gcp), ex:

  % recuperer les propriétes du pool courant:  
   p = gcp('nocreate');
fprintf('le nombre de workers est: %d \n',p.NumWorkers);  
   

%delete(gcp('nocreate'))

% merci de quitter matlab pour libérer les jetons de licence.
quit 
