#!/bin/bash

#-------------------  CHECKPOINT  --------------------------- 
#
#  exemple de programme "trop long" qui utilise la fonctionnalité
#  "checkpoint" d'OAR, suivi de reprise(s) automatique(s)
#  (fonctionnalité idempotent) ... jusqu' à la fin du programme 

# commande: oarsub -S ./checkpoint-user1.oar

# Cet exemple est arrêté 1 fois ( 2 jobs consécutifs pour finir)

#
#  IMPORTANT:
#  Ce script OAR capte bien le signal  de  checkpoint envoyé par
#  OAR, envoit un signal (à adapter éventuellement) au programme de
#  de l'utilisateur, MAIS c'est LE PROGRAMME de l'utilisateur qui
#  doit capter et interpréter ce signal ... afin de sauver le
#  "contexte" de son execution de sorte qu'au remédérrage
#  automatique ( - t idempotent ), il ne reprenne pas du début
#  ( sinon il y a une boucle infinie !)
#
#  Le programme associé à ce script est un simple compteur
#   ( 1 incrémentation / secondes ) en C.
#   - son exécution (normal) dure 4 mn
#   - ce script OAR, pour l'exemple, ne prévoit que 3mn30 (trop court!) 
#   - le script OAR reçoit (d'OAR) le signal checkpoint 12.
#     Il renvoit le signal 2 (SIGINT) au programme timer
#   - À réception de ce dernier (interruption) le timer sauve
#     aussitôt son contexte (le n° d'itération) dans un fichier local. 
#   - timer10mn est arrêté mais est relancé par OAR
# 
#-------------- paramètres OAR---------------------------
#OAR -l cpu=1/core=1,walltime=00:3:30 
#OAR -n checkpoint-user1

# Les fichiers de sortie sans paramètres $OAR_JOBID
# (=> les jobs successifs écrivent dans les mêmes fichiers)
#OAR -O OAR-ckpt-user1.out
#OAR -E OAR-ckpt-user1.err

#OAR -q default

## besteffort est optionnel 
#OAR -t besteffort
## idempotent ne l'est pas 
#OAR -t idempotent

# checkpoint 60 s avant le walltime: pour un exemple
# réel ( plusieurs heures, gros contexte, 600 (10mn))  
#OAR --checkpoint 60
# signal envoyé par OAR (SIGUSR2) 
#OAR --signal 12 


#---------- la tâche à exécuter   ------------------------ 
# compilation ( peut être commenté si déjà compilé) 
gcc compteur4mn.c  -o timer4mn
# Note (compteur4mn.c): gère son propre checkpoint (SIGINT)
#----------- pré-traitement -----------------------------
# DWTFYW !
echo "Debut du job ${OAR_JOB_ID}"

#----------- lancement de la tâche ----------------------
./timer4mn &

#---------------------------------------------------------
# gestion de signaux
#---------------------------------------------------------
# Warning: c'est le script OAR qui reçoit le signal 12!
#  => il faut le capter et le transmettre au PROG
####################################################

# pid du programme = pid de la dernière commande (: ./timer10mn)

PROGPID=$!

echo "le PROG PID est : $PROGPID"

# fonction de traitement du signal OAR
sighandler() {
    echo "reception checkpoint OAR (signal12)"
    # demande de checkpoint (-c) au programme
    # SIGINT = 2 = "CRTL + C" (cf compteur10mn.c)
    kill -s 2 $PROGPID
    # IMPORTANT : pause
    sleep 4
    [ -n "$PROGPID" ] && kill -9 $PROGPID
    SCRIPT_CHECKPOINTED="YES"
    echo "checkpoint : $SCRIPT_CHECKPOINTED"
    }

# signal Unix attendu par OAR (defaut = SIGUSR1/10, forcé ici
# à 12 par le paramètre --signal 12 (cf. param OAR au début)
CHKPNT_SIGNAL=12

# valeur de sortie pour l'option "--idempotent"
# (restart automatique) )
EXIT_UNFINISHED=99

# si le script OAR reçoit le SIGUSR2 (signal 12), checkpoint!

# indique au script OAR de capter le signal 12 
trap sighandler  $CHKPNT_SIGNAL

# wait indispensable pour maintien des communucations
# (signaux) entre ce script et le PROG
while kill -0 "$PROGPID" 2>/dev/null; do
    wait $PROGPID
done

[ -n "$SCRIPT_CHECKPOINTED" ] && exit 99

#---------------------------------------------------------
# fin du programme:  postraitement
#---------------------------------------------------------
# DWTFYW!
echo " ---post treatement---"
echo " ...suppression du fichier temporaire context_compteur.txt"
rm -f context_compteur.txt
echo " c'est FINI ! "
# => idempotent devrait entraîner un redémarrage auto.
exit $?
