Project: admin PCS
author: Ph. M.
date: 2016-10-18, update 2017-11-12
title: Checkpoint



## Contenu

### scripts OAR  de la documentation en ligne

- exemples de scripts _OAR_ de lancement des batchs de la fiche [«tutorial checkpoint»](https://www-calculco.univ-littoral.fr/readthedocs/calculs/checkpoint/) sur la plateforme [Calculco](https://www-calculco.univ-littoral.fr)
- les programmes compteurs (C, python, matlab) associé à ce tutorial

Note: mise en œuvre de  DMTCP (Distributed MultiThreaded Checkpointing) http://dmtcp.sourceforge.net/


### autres exemples et tests

- **OpenMP**: 
    - checkpoint-dmtcp-openmp4.oar (avec le programme timing-matmul2000.f90)
	- checkpoint-dmtcp-openmp5.oar (avec le programme timing-matmul2000.f90)

- **Scratch**: checkpoint-dmtcp-scratch-ex6.oar (scratch-ex6.tgz) : les calculs sont effectués sur l'espace local (scratch). 
