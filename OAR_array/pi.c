#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// Approximation de pi par le calcul de l'integrale:  integral(4./(1+x.^2), 0, 1)

// le nombre d'intervalles (entier!) est lu en argument du programme (pour illuster
//  un exemple de lancement de batch OAR --array sur calculco) 

// Fonction a integrer pour le calcul de pi
double f(double a) {
  return 4/(1 + a*a);
}

int main(int argc, char **argv) {
  if (argc < 2) // par d'argumenteno arguments were passed
    {
      fprintf(stderr, "usage : entrer le nombre d'intervalles en argument, ex: ./pi 10000 .\n");
      return EXIT_FAILURE;
    }
  else
    {

      //int n=10;
      // le nombre d'intervalles est lu en argument (entier!)
      int n=atoi( argv[1] );
      double Pi_c;
      // Longueur de l'intervalle d'integration
      double h = 1.0 / n;
      
      Pi_c = 0;
      for(int i = 0; i<n; i++) {
	double x = h * (i + 0.5);
	Pi_c += f(x);
      }
      Pi_c = h * Pi_c;
      
      //valeur estimee de Pi par arcosinus(-1)
      double Pi_e = acos(-1);
      // écart entre les deux valeurs
      double ecart = fabs(Pi_e - Pi_c);
      
      fprintf(stdout, "\n\n"
	      "   nb d'intervalles     : %d\n"
	      "   Pi_c                 : %2.10E\n"	 
	      "  |Pi_e - Pi_calculé|   : %10.4E\n",
	      n, Pi_c, ecart
	      );
      return EXIT_SUCCESS;
    }
}


