% pi_checkpoint_ex5.m:
%    - Calcul de l'approximation de pi par la m�thode de Leibniz
%    - le programme simule un job long  avec des instructions "pause" inutiles
%  avec un checkpoint � intevalle de temps r�gulier. 
%    - Il s'agit juste d'un exemple ...le choix de placer deux tests "if", une 
%   �ventuelle sauvegarde dans une boucle qui contient seulement 3 instructions 
%   de calculs �l�mentaire est �videmment tr�s critiquable!
%
%    - Le probl�me des checkpoint est � prendre en consid�ration si vous soumettez
%    des jobs de tr�s longue dur�e sur le cluster ( pannes , coupures de courant,
%    jobs soumis sur des files besteffort, long ,...)
% 
% Note: le script pi_checkpoint_ex5.m peut-�tre tester avec Matlab en mode
% interactif:  il suffit de le stopper avec CTRL+C (et le relancer une 2i�me fois)
%

if exist('check.mat')
    % initialise les donn�es au red�marrage
    fprintf('restart apr�s arr�t\n');  
    load('check.mat');  
    fprintf('valeur provisoire de pi: %f et iteration %d \n',4*approxpi,n);
    % modif du flag 'passe' pour supprimer les pauses et finir le calcul
    passe = 0;
    save('check.mat');
else
    Nmax=1000;
    approxpi=0;
    fac=1;
    n=1;
    passe=1;
    save('check.mat','Nmax','approxpi','fac','n','passe');
end 
t = clock;
% environ 30 secondes entre chaque checkpoint
intervalle_checkpnt=30;
while( n < Nmax )
    approxpi=approxpi+fac/(2*n-1);
    fac=-fac;
    n=n+1;
    % simulation d'un job long...
    if ( passe == 1 )
        pause(2)
    end
    % checkpoint
    if( etime(clock,t) >= intervalle_checkpnt )           
        fprintf('checkpoint, indice de boucle = %d \n', n );  
        save('check.mat','Nmax','approxpi','fac','n','passe');
        %pause( 1 );
        t = clock;
    end
end
approxpi=4*approxpi;
fprintf('\n approximation de pi: %f apr�s %d it�rations\n',approxpi,Nmax); 
% suppression du fichier checkpoint
delete('check.mat');

% merci de quitter matlab pour lib�rer les jetons de licence.
 quit 
