function [x] = testgradientconj(n,tol,maxiter)

% exemple pour illustrer un exemple de passage de paramètres
% d'un script matlab lancer en mode batch sur calculco (tutoriaux)

% résolution du système Ax =b par la méthode du gradient conjugué

% En entrée les arguments (facultatifs!) sont:
%   -n : taille du système  (100 par défaut)
%   -tol: la tolérance  (1-e5 par défaut)
%   -maxiter: nombre max d'itérations (n, par défaut)

% En sortie : x la solution ... qui est connue (!): ones(n,1) 

if nargin < 1 
    n = 100; 
end
if nargin < 2 
    tol = 1e-5; 
end
if nargin < 3
    maxiter = n; 
end
Atmp=rand(n,n);
A = Atmp + Atmp';
sol=ones(n,1);
b=A*sol;
x0 = rand(n,1);
iter =1;
% Calcul du 1er résidu
r = A*x0-b;
% direction du gradient d = -r
d = -r;
% Calcul du pas 
a = norm(r)^2/dot(A*d,d);
% 1ere itération x1
x_ = x0 + d*a;
% gradient en x1
r_ = A*x_- b;
p = norm(r_)^2/dot(r_-r,d);
% direction 
d_ = -r_+ d*p;
x = x_;
r = r_;
d = d_;
while ((norm(r) > tol) && (iter < maxiter))
    a = norm(r)^2/dot(A*d,d);
    x_ = x + d*a;
    r_ = A*x_-b;
    p = norm(r_)^2/dot(d,r_-r);
    % direction suivante
    d_ = -r_+ d*p; 
    x = x_;
    r = r_;
    d = d_;
    iter=iter+1;
end
%nombre d'iterations
  fprintf('Les 10 premiers elements de la solution sont : \n')
  x(1:10)
  fprintf('Le nombre d''iterations realisee est : %d \n', iter);

end
