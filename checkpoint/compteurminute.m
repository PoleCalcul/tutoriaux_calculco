function [x] = compteurminute(n)
i=1;
%max=60;
if nargin < 1
    n=2;
end

fid = fopen('compteur_matlab.out','w');
while i < n*60
  fprintf(fid,'%2d secondes\n',i);
  %fprintf(" i = %d \n",i);
  i = i + 1;
  pause(1);
end
fclose(fid);
end

